import 'package:flutter/material.dart';
import 'package:guia_bolso/screens/information_screen.dart';
import 'package:guia_bolso/src/informations.dart';
import 'package:transparent_image/transparent_image.dart';

class InformationsTile extends StatelessWidget {

  final Informations information;
  InformationsTile(this.information);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => InformationScreen(information)));
      },
      child: Card(
        color: Color(0xFF304FFE),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        elevation: 4,
        child: Column(
          children: <Widget>[
            Hero(
              tag: "store " + information.titulo,
              child: ClipRRect(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(6), topRight: Radius.circular(6)),
                child: FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: information.img,
                ),
              ),
            ),

            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 20),
                child: Text(
                  information.titulo.toUpperCase(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),


            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 10,  right: 20),
                child: Text(
                  information.desc,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.left,
                  maxLines: 3,
                ),
              ),
            ),

            ButtonTheme.bar(
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => InformationScreen(information)));
                    },
                    child: const Text(
                      "VEJA MAIS",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
