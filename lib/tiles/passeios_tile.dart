import 'package:flutter/material.dart';
import 'package:guia_bolso/screens/passeio_screen.dart';
import 'package:guia_bolso/src/passeio.dart';
import 'package:transparent_image/transparent_image.dart';

class PasseiosTile extends StatelessWidget {

  final Passeio passeio;
  PasseiosTile(this.passeio);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => PasseioScreen(passeio)));
      },
      child: Card(
        color: Color(0xFF304FFE),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        elevation: 4,
        child: Column(
          children: <Widget>[
            Hero(
              tag: "store " + passeio.name,
              child: ClipRRect(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(6), topRight: Radius.circular(6)),
                child: FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: passeio.img,
                ),
              ),
            ),

            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 20),
                child: Text(
                  passeio.name.toUpperCase(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),


            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 10,  right: 20),
                child: Text(
                  passeio.desc,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.left,
                  maxLines: 3,
                ),
              ),
            ),

            ButtonTheme.bar(
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                    onPressed: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => PasseioScreen(passeio)));
                    },
                    child: const Text(
                      "VEJA MAIS",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
