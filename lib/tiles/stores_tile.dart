import 'package:flutter/material.dart';
import 'package:guia_bolso/screens/store_screen.dart';
import 'package:guia_bolso/src/stores.dart';
import 'package:transparent_image/transparent_image.dart';


class StoreTile extends StatelessWidget {

  final Store store;
  StoreTile(this.store);


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => StoreScreen(store)));
      },
      child: Card(
        color: Color(0xFF304FFE),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(6),
        ),
        elevation: 4,
        child: Column(
          children: <Widget>[
            Hero(
                tag: "store " + store.name,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(6), topRight: Radius.circular(6)),
                  child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: store.img,
                  ),
                ),
            ),

            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 20),
                child: Text(
                  store.name.toUpperCase(),
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),


            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 20, top: 10,  right: 20),
                child: Text(
                  store.desc,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                  textAlign: TextAlign.left,
                  maxLines: 3,
                ),
              ),
            ),

            ButtonTheme.bar(
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => StoreScreen(store)));
                      },
                      child: const Text(
                          "VEJA MAIS",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
