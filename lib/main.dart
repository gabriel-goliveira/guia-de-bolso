import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:guia_bolso/screens/home_screen.dart';

import 'bloc/cards_places_bloc.dart';
import 'bloc/cards_tools_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      blocs: [
        Bloc((i) => CardPlacesBloc()),
        Bloc((j) => CardToolsBloc()),
      ],
      child: MaterialApp(
        title: 'Guia de Bolso',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Color(0xFF304FFE),
          hintColor: Colors.white,
          inputDecorationTheme: InputDecorationTheme(
            enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFF304FFE))),
            disabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFF304FFE))),
          ),
          unselectedWidgetColor: Colors.white,
        ),
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
        routes: {
          '/home': (context) => HomeScreen(),
        },
      ),
    );
  }
}
