import 'package:flutter/material.dart';
import 'package:guia_bolso/src/stores.dart';
import 'package:guia_bolso/widget/rating_bar.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';

class StoreScreen extends StatelessWidget {

  final Store store;
  StoreScreen(this.store);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
              child: Column(
                children: <Widget>[
                  Hero(
                    tag: "store " + store.name,
                    child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: store.img,
                    ),
                  ),
                ],
              )
      ),
          Positioned.fill(
              child: Padding(
                  padding: EdgeInsets.only(
                   top: MediaQuery.of(context).size.height/3,
                  ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(15),
                                topRight: Radius.circular(15)
                            )
                        ),
                        child: SingleChildScrollView(
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 30.0,
                              left: 20.0,
                              right: 20.0,
                            ),
                            child: Column(
                              children: <Widget>[
                                Align(
                                  alignment: Alignment.topLeft,
                                  child:  Text(
                                    store.name,
                                    style: TextStyle(
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.bold,
                                      fontSize: 28.0,
                                      color: Colors.grey[800],
                                    ),
                                  ),
                                ),

                                SizedBox(height: 10,),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    RatingBar(rating: store.rating,),
                                    Text(
                                      store.rating.toString(),
                                      style: TextStyle(
                                        color: Colors.grey.withOpacity(0.6),
                                        fontFamily: "Roboto",
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18.0,
                                      ),
                                    )
                                  ],
                                ),

                                SizedBox(height: 15,),

                                Align(
                                  alignment: Alignment.topLeft,
                                  child:  Text(
                                    store.category,
                                    style: TextStyle(
                                      fontFamily: "Roboto",
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18.0,
                                      color: Colors.grey[700],
                                    ),
                                  ),
                                ),

                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Text(
                                    store.text,
                                    style: TextStyle(
                                      fontFamily: "Roboto",
                                      letterSpacing: 1,
                                      height: 1.2,
                                      fontSize: 18.0,
                                      color: Colors.grey[700],
                                    ),
                                  ),
                                ),



                                Align(
                                  alignment: Alignment.topLeft,
                                  child: RichText(
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: "Endereço: ",
                                            style: TextStyle(
                                              fontFamily: "Roboto",
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0,
                                              color: Colors.grey[700],
                                            ),
                                          ),
                                          TextSpan(
                                            text: store.address,
                                            style: TextStyle(
                                              fontFamily: "Roboto",
                                              fontSize: 16.0,
                                              color: Colors.grey[700],
                                            ),
                                          )
                                        ],
                                      ),
                                  ),
                                ),

                                SizedBox(height: 15,),

                                Align(
                                  alignment: Alignment.topLeft,
                                  child: RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: "Horário de atendimento: ",
                                          style: TextStyle(
                                            fontFamily: "Roboto",
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.0,
                                            color: Colors.grey[700],
                                          ),
                                        ),
                                        TextSpan(
                                          text: store.hour,
                                          style: TextStyle(
                                            fontFamily: "Roboto",
                                            fontSize: 16.0,
                                            color: Colors.grey[700],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),


                                SizedBox(height: 30,),

                                Align(
                                  alignment: Alignment.bottomCenter,
                                  child: SizedBox(
                                    height: 40,
                                    child: RaisedButton.icon(
                                        color: Color(0xFF304FFE),
                                        onPressed: () async{
                                          if (await canLaunch(store.site)) {
                                            await launch(store.site);
                                          } else {
                                            throw 'Could not launch ${store.site}';
                                          }
                                        },
                                        icon: Icon(
                                          Icons.launch,
                                          color: Colors.white,
                                        ),
                                        label: Text(
                                          "VISITE O SITE",
                                          style: TextStyle(color: Colors.white),
                                        )
                                    ),
                                  )
                                ),

                                SizedBox(height: 25,),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
          ),
        ],
      ),
    );
  }
}
