import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:guia_bolso/bloc/cards_places_bloc.dart';
import 'package:guia_bolso/bloc/cards_tools_bloc.dart';
import 'package:guia_bolso/data_img/data_img.dart';
import 'package:guia_bolso/data_img/data_tools.dart';
import 'package:guia_bolso/screens/passeios_screen.dart';
import 'package:guia_bolso/screens/stores_screen.dart';
import 'package:guia_bolso/widget/card_scrol.dart';
import 'package:guia_bolso/widget/card_tools.dart';
import 'calculadora_screen.dart';
import 'conversor_menu_screen.dart';
import 'informations_screen.dart';
import 'lista_screen.dart';
import 'outlets_screen.dart';
import 'restaurantes_screen.dart';


class HomeScreen extends StatelessWidget {


    @override
    Widget build(BuildContext context) {
      final bloc = BlocProvider.getBloc<CardPlacesBloc>();
      final bloc2 = BlocProvider.getBloc<CardToolsBloc>();


      var currentPage = images.length - 1.0;
      var currentPageTools = imagesTools.length - 1.0;

      PageController controller = PageController(
          initialPage: images.length - 1);
      PageController controller2 = PageController(
          initialPage: imagesTools.length - 1);

      controller.addListener(() {
          currentPage = controller.page;
          bloc.inPlaces.add(currentPage);
      });

      controller2.addListener(() {
          currentPageTools = controller2.page;
          bloc2.inTools.add(currentPageTools);
      });

      return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF2d3447),
          elevation: 0,
        ),
        backgroundColor: Color(0xFF2d3447),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Guia de Bolso",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 46,
                        letterSpacing: 1,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, top: 10),
                child: Row(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xFFff6e6e),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 22, vertical: 6),
                          child: Text(
                            "Disney",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              StreamBuilder(
                initialData: [],
                  stream: bloc.outPlaces,
                  builder: (context, snapshot){
                    if(snapshot.hasData){
                     return Stack(
                        children: <Widget>[
                          CardScrolWidget(currentPage: currentPage,),
                          Positioned.fill(
                            child: PageView.builder(
                                itemCount: images.length,
                                controller: controller,
                                reverse: true,
                                itemBuilder: (context, index) {
                                  return GestureDetector(
                                    onTap: (){
                                      if(currentPage==4.0){
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=> StoresScreen()));
                                      }else if(currentPage==3.0){
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => OutletsScreen()));
                                      }else if(currentPage==2.0){
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => PasseiosScreen()));
                                      }else if(currentPage==1.0){
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => RestaurantesScreen()));
                                      }else if(currentPage==0.0){
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) =>InformationsScreen()));
                                      }
                                    },
                                  );
                                }
                            ),
                          ),
                        ],
                      );
                    }else{
                      return Center(child: CircularProgressIndicator(),);
                    }
                  }
              ),


              // Ferramentas ==========================================================================================

              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Ferramentas",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 46,
                        letterSpacing: 1,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20, top: 10),
                child: Row(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.blueAccent,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 22, vertical: 6),
                          child: Text(
                            "Tools",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
             StreamBuilder(
               initialData: [],
                 stream: bloc2.outTools,
                 builder: (context,snapshot){
                  if(snapshot.hasData){
                   return  Stack(
                      children: <Widget>[
                        CardToolsWidget(currentPage: currentPageTools,),
                        Positioned.fill(
                          child: PageView.builder(
                              itemCount: imagesTools.length,
                              controller: controller2,
                              reverse: true,
                              itemBuilder: (context, index) {
                                return GestureDetector(
                                  onTap: (){
                                    if(currentPageTools == 2.0){
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => ListaScreen()));
                                    }else if(currentPageTools==0.0){
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => CalculadoraScreen()));
                                    }else if(currentPageTools==1.0){
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => ConversorMenu()));
                                    }
                                  },
                                );
                              }
                          ),
                        ),
                      ],
                    );
                  }else{
                    return Center(child: CircularProgressIndicator(),);
                  }
                 }
             ),
            ],
          ),
        ),
      );
    }
  }
