import 'package:flutter/material.dart';
import 'package:guia_bolso/src/informations.dart';
import 'package:guia_bolso/tiles/informations_tile.dart';

class InformationsScreen extends StatelessWidget {

  List informacoes = informations;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF2d3447),
          elevation: 0,
        ),
        backgroundColor: Color(0xFF2d3447),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Restaurantes",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 46,
                      letterSpacing: 1,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                child: Container(
                  child: ListView.builder(
                      itemCount: informacoes.length,
                      itemBuilder: (context, index) {
                        return InformationsTile(informacoes[index]);
                      }),
                ),
              ),
            ),
          ],
        )
    );
  }
}
