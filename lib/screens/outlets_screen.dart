import 'package:flutter/material.dart';
import 'package:guia_bolso/src/outlets.dart';
import 'package:guia_bolso/tiles/outlets_tile.dart';

class OutletsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    List shoppings = outlets;

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF2d3447),
          elevation: 0,
        ),
        backgroundColor: Color(0xFF2d3447),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Outlets/Shoppings",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 36,
                      letterSpacing: 1,
                      fontFamily: "Roboto",
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                child: Container(
                  child: ListView.builder(
                      itemCount: shoppings.length,
                      itemBuilder: (context, index) {
                        return OutletsTile(shoppings[index]);
                      }),
                ),
              ),
            ),
          ],
        )
    );
  }
}
