import 'package:flutter/material.dart';
import 'package:guia_bolso/src/passeio.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:url_launcher/url_launcher.dart';

class PasseioScreen extends StatelessWidget {

  final Passeio passeio;
  PasseioScreen(this.passeio);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
              height: MediaQuery.of(context).size.height,
              child: Column(
                children: <Widget>[
                  Hero(
                    tag: "store " + passeio.name,
                    child: FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: passeio.img,
                    ),
                  ),
                ],
              )
          ),
          Positioned.fill(
            child: Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height/3,
              ),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(15),
                              topRight: Radius.circular(15)
                          )
                      ),
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: const EdgeInsets.only(
                            top: 30.0,
                            left: 20.0,
                            right: 20.0,
                          ),
                          child: Column(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.topLeft,
                                child:  Text(
                                  passeio.name,
                                  style: TextStyle(
                                    fontFamily: "Roboto",
                                    fontWeight: FontWeight.bold,
                                    fontSize: 28.0,
                                    color: Colors.grey[800],
                                  ),
                                ),
                              ),


                              SizedBox(height: 25,),


                              Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  passeio.text,
                                  style: TextStyle(
                                    fontFamily: "Roboto",
                                    letterSpacing: 1,
                                    height: 1.2,
                                    fontSize: 18.0,
                                    color: Colors.grey[700],
                                  ),
                                ),
                              ),

                              SizedBox(height: 20,),


                              Align(
                                alignment: Alignment.topLeft,
                                child: RichText(
                                  text: TextSpan(
                                    children: [
                                      TextSpan(
                                        text: "Endereço: ",
                                        style: TextStyle(
                                          fontFamily: "Roboto",
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                          color: Colors.grey[700],
                                        ),
                                      ),
                                      TextSpan(
                                        text: passeio.address,
                                        style: TextStyle(
                                          fontFamily: "Roboto",
                                          fontSize: 16.0,
                                          color: Colors.grey[700],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),


                              SizedBox(height: 30,),

                              Align(
                                  alignment: Alignment.bottomCenter,
                                  child: SizedBox(
                                    height: 40,
                                    child: RaisedButton.icon(
                                        color: Color(0xFF304FFE),
                                        onPressed: () async{
                                          if (await canLaunch(passeio.site)) {
                                            await launch(passeio.site);
                                          } else {
                                            throw 'Could not launch ${passeio.site}';
                                          }
                                        },
                                        icon: Icon(
                                          Icons.launch,
                                          color: Colors.white,
                                        ),
                                        label: Text(
                                          "VISITE O SITE",
                                          style: TextStyle(color: Colors.white),
                                        )
                                    ),
                                  )
                              ),

                              SizedBox(height: 25,),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
