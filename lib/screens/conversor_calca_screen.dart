import 'package:flutter/material.dart';

class ConversorCalca extends StatefulWidget {
  @override
  _ConversorCalcaState createState() => _ConversorCalcaState();
}

class _ConversorCalcaState extends State<ConversorCalca> {


  final tamanhoController = TextEditingController();
  final masculinoController = TextEditingController();
  final femininoController = TextEditingController();


  Map<String, dynamic> tamanhosMasculinos ={
    "34" : "24",
    "36" : "26",
    "38" : "28",
    "40" : "30",
    "42" : "32",
    "44" : "34",
    "46" : "36",
    "48" : "38",
    "50" : "40",
    "52" : "42",
    "54" : "44",
    "56" : "46",
  };

  Map<String, dynamic> tamanhosFemininos ={
    "34" : "0",
    "36" : "2",
    "38" : "4",
    "40" : "6",
    "42" : "8",
    "44" : "10",
    "46" : "12",
    "48" : "14",
    "50" : "16",
    "52" : "18",
    "54" : "20",
    "56" : "22",
  };


  void _valorChanged(String text){
    if(text.isEmpty){
      masculinoController.text = "";
      femininoController.text = "";
    }else{
      var tam = text.toUpperCase();
      if(tamanhosMasculinos.containsKey(tam)){
        masculinoController.text = tamanhosMasculinos[tam];
        femininoController.text = tamanhosFemininos[tam];
      }else{
        masculinoController.text = "";
        femininoController.text = "";
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF2d3447),
        elevation: 0,
      ),
      backgroundColor: Color(0xFF2d3447),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Icon(Icons.language, size: 150.0, color: Theme.of(context).primaryColor,),
            Padding(
              padding: EdgeInsets.only(top: 30.0),
              child: Form(
                  child: Column(
                    children: <Widget>[
                      Divider(),
                      buildTextField("Tamanho", tamanhoController, _valorChanged),

                      Divider(),
                      TextField(
                        controller: masculinoController,
                        decoration: InputDecoration(
                          labelText: "Masculino",
                          labelStyle: TextStyle(color: Colors.white),
                          border: OutlineInputBorder(),
                          prefixStyle: TextStyle(color: Colors.white),
                        ),
                        style: TextStyle(
                            color: Colors.white, fontSize: 15.0
                        ),
                        enabled: false,
                        keyboardType: TextInputType.number,
                      ),

                      Divider(),
                      TextField(
                        controller: femininoController,
                        decoration: InputDecoration(
                          labelText: "Feminino",
                          labelStyle: TextStyle(color: Colors.white),
                          border: OutlineInputBorder(),
                          prefixStyle: TextStyle(color: Colors.white),
                        ),
                        style: TextStyle(
                            color: Colors.white, fontSize: 15.0
                        ),
                        enabled: false,
                        keyboardType: TextInputType.number,
                      ),


                      Padding(
                        padding: EdgeInsets.only(top: 40),
                        child: Align(
                            alignment: Alignment.bottomCenter,
                            child: SizedBox(
                              width: double.infinity,
                              height: 60,
                              child: RaisedButton.icon(
                                  color: Color(0xFF304FFE),
                                  onPressed: () {
                                    tamanhoController.text = "";
                                    femininoController.text = "";
                                    masculinoController.text = "";
                                  },
                                  icon: Icon(
                                    Icons.refresh,
                                    color: Colors.white,
                                  ),
                                  label: Text(
                                    "LIMPAR",
                                    style: TextStyle(color: Colors.white),
                                  )
                              ),
                            )
                        ),
                      ),
                    ],
                  )

              ),
            ),
          ],
        ),
      ),
    );
  }
}


Widget buildTextField(String label, TextEditingController controller, Function f){
  return  TextField(
    controller: controller,
    decoration: InputDecoration(
      labelText: label,
      labelStyle: TextStyle(color: Colors.white),
      border: OutlineInputBorder(),

    ),
    style: TextStyle(
        color: Colors.white, fontSize: 15.0
    ),
    onChanged: f,
    keyboardType: TextInputType.number,
  );
}