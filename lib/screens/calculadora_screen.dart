import 'package:flutter/material.dart';

class CalculadoraScreen extends StatefulWidget {
  @override
  _CalculadoraScreenState createState() => _CalculadoraScreenState();
}

class _CalculadoraScreenState extends State<CalculadoraScreen> {

  final valorController = TextEditingController();
  final impostoController = TextEditingController();
  final resultController = TextEditingController();


  void _valorChanged(String text){
    if(text.isEmpty){
      impostoController.text = "";
      resultController.text = "";
    }else{
      double valor = double.parse(text);
      double imposto = (valor*0.065);
      double total = (valor*1.065);
      impostoController.text = imposto.toStringAsFixed(2);
      resultController.text = total.toStringAsFixed(2);
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF2d3447),
        elevation: 0,
      ),
      backgroundColor: Color(0xFF2d3447),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Icon(Icons.monetization_on, size: 150.0, color: Theme.of(context).primaryColor,),
            Padding(
              padding: EdgeInsets.only(top: 30.0),
              child: Form(
                  child: Column(
                    children: <Widget>[
                      Divider(),
                      buildTextField("Valor", "US\$", valorController, _valorChanged),

                      Divider(),
                      TextField(
                        controller: impostoController,
                        decoration: InputDecoration(
                          labelText: "Imposto",
                          labelStyle: TextStyle(color: Colors.white),
                          border: OutlineInputBorder(),
                          prefixText: "US\$",
                          prefixStyle: TextStyle(color: Colors.white),
                        ),
                        style: TextStyle(
                            color: Colors.white, fontSize: 15.0
                        ),
                        enabled: false,
                        keyboardType: TextInputType.number,
                      ),

                      Divider(),
                      TextField(
                        controller: resultController,
                        decoration: InputDecoration(
                            labelText: "Valor Total",
                            labelStyle: TextStyle(color: Colors.white),
                            border: OutlineInputBorder(),
                            prefixText: "US\$",
                            prefixStyle: TextStyle(color: Colors.white),
                        ),
                        style: TextStyle(
                            color: Colors.white, fontSize: 15.0
                        ),
                        enabled: false,
                        keyboardType: TextInputType.number,
                      ),


                      Padding(
                          padding: EdgeInsets.only(top: 40),
                        child: Align(
                            alignment: Alignment.bottomCenter,
                            child: SizedBox(
                              width: double.infinity,
                              height: 60,
                              child: RaisedButton.icon(
                                  color: Color(0xFF304FFE),
                                  onPressed: () {
                                    valorController.text = "";
                                    impostoController.text = "";
                                    resultController.text = "";
                                  },
                                  icon: Icon(
                                    Icons.refresh,
                                    color: Colors.white,
                                  ),
                                  label: Text(
                                    "LIMPAR",
                                    style: TextStyle(color: Colors.white),
                                  )
                              ),
                            )
                        ),
                      ),
                    ],
                  )

              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildTextField(String label, String prefixo, TextEditingController controller, Function f){
  return  TextField(
    controller: controller,
    decoration: InputDecoration(
        labelText: label,
        labelStyle: TextStyle(color: Colors.white),
        border: OutlineInputBorder(),
        prefixText: prefixo
    ),
    style: TextStyle(
        color: Colors.white, fontSize: 15.0
    ),
    onChanged: f,
    keyboardType: TextInputType.number,
  );
}
