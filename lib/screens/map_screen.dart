import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:guia_bolso/src/outlets.dart';
import 'package:transparent_image/transparent_image.dart';

class MapScreen extends StatefulWidget {

  final Outlets outlet;
  MapScreen(this.outlet);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  @override
  initState() {
    SystemChrome.setEnabledSystemUIOverlays([]);
    super.initState();
  }

  @override
  void dispose() {
    //SystemChrome.restoreSystemUIOverlays();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        body: Center(
          child: Hero(
              tag: 'map ${widget.outlet.map}',
              child: FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: widget.outlet.map,
              ),
        ),
        ),
      ),
      onTap: () {
        Navigator.pop(context);
      },
    );
  }
}
