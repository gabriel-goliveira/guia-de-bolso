import 'package:flutter/material.dart';
import 'package:guia_bolso/screens/stores_screen.dart';

/// List of places images path
List<String> images = [
  "images/informacoes.jpg",
  "images/restaurante.jpg",
  "images/passeios.jpg",
  "images/shopping.jpg",
  "images/lojas.jpg",
];

///List of places titles
List<String> titles = [
  "Informações",
  "Restaurantes",
  "Passeios",
  "Outlets/Shoppings",
  "Lojas",
];
