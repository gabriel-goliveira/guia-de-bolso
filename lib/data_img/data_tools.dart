import 'package:flutter/material.dart';

/// List of tools images path
List<String> imagesTools = [
  "images/conversor.jpg",
  "images/medidas.jpg",
  "images/lista.jpg",
];

///List of tools titles
List<String> tools = [
  "Calculadora de Imposto",
  "Conversor de Medidas",
  "Lista de Compras",
];

final routes = [];
