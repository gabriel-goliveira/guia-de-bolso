import 'package:bloc_pattern/bloc_pattern.dart';
import 'dart:async';

class CardToolsBloc implements BlocBase {
  final StreamController<dynamic> _toolsController =
      StreamController<dynamic>();

  ///Show currentPage tools
  Stream get outTools => _toolsController.stream;

  ///Receive currentPage tools
  Sink get inTools => _toolsController.sink;

  CardToolsBloc();

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _toolsController.close();
  }

  @override
  // TODO: implement hasListeners
  bool get hasListeners => null;

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(listener) {
    // TODO: implement removeListener
  }
}
