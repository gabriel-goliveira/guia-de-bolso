import 'package:bloc_pattern/bloc_pattern.dart';
import 'dart:async';

class CardPlacesBloc implements BlocBase {
  final StreamController<dynamic> _placeController =
      StreamController<dynamic>();

  ///Show currentPage places
  Stream get outPlaces => _placeController.stream;

  ///Receive currentPage places
  Sink get inPlaces => _placeController.sink;

  CardPlacesBloc() {
    //_placeController.stream.listen(_getPlaces);
  }

  @override
  void addListener(listener) {
    // TODO: implement addListener
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _placeController.close();
  }

  @override
  // TODO: implement hasListeners
  bool get hasListeners => null;

  @override
  void notifyListeners() {
    // TODO: implement notifyListeners
  }

  @override
  void removeListener(listener) {
    // TODO: implement removeListener
  }
}
