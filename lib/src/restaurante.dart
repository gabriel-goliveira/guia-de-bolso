class Restaurante {
  ///Image link to get online image
  final String img;

  ///Restaurant name
  final String nome;

  ///Restaurant princes review
  final String valor;

  ///Restaurant description
  final String desc;

  ///Restaurant post content
  final String text;

  ///Restaurant address
  final String address;

  ///Restaurant weblink
  final String site;

  const Restaurante(
      {this.img,
      this.nome,
      this.valor,
      this.desc,
      this.text,
      this.address,
      this.site});
}

///Restaurant object list
final restaurantes = [
  Restaurante(
      img:
          "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando-olive-garden.jpg",
      nome: "Olive Garden",
      valor: "Preço justo(US\$10-25 por pessoa)",
      desc:
          "O cardápio do Olive Garden é variado, mas o ponto forte são as massas. ",
      text:
          "O cardápio do Olive Garden é variado, mas o ponto forte são as massas. "
          "O pãozinho servido na entrada como cortesia também é muito elogiado, e bebidas não alcoólicas são vendidas no modo refil (você paga um copo e bebe o quanto quiser). \n"
          "Pratos de destaque são o Fettuccine Alfredo e o Spaghetti with Meat Sauce, que é o famoso macarrão a bolonhesa de domingo."
          "Daí, é só pedir pro garçom trazer “parmesan” que a festa tá feita.",
      address: "12361 State Road 535 – Orlando, FL 32836",
      site: "https://www.olivegarden.com/home"),
  Restaurante(
      img:
          "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando-cheescake-factory.jpg",
      nome: "Cheesecake Factory",
      valor: "Barato - Preço justo(US\$15-30 por pessoa)",
      desc:
          "A Cheesecake Factory é uma rede de restaurantes super conhecida nos Estados Unidos e está presente praticamente no país inteiro.",
      text:
          "A Cheesecake Factory é uma rede de restaurantes super conhecida nos Estados Unidos e está presente praticamente no país inteiro.\n"
          "Os preços variam porque se você quiser consumir somente as famosas e deliciosas sobremesas, "
          "a conta sairá um pouco mais barata em relação a uma refeição completa, que também é muito elogiada e variada.\n"
          "Prato de destaque é o Godiva Chocolate Cheesecake",
      address: "4200 Conroy Rd, Orlando, FL 32839 - Dentro do Shopping",
      site: "https://www.thecheesecakefactory.com/"),
  Restaurante(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando_perkins.jpg",
    nome: "Perkins",
    valor: "Barato(US\$10-20 por pessoa)",
    desc:
        "Comida tipicamente americana, servidas no café da manhã, almoço e jantar.",
    text:
        "Comida tipicamente americana, servidas no café da manhã, almoço e jantar. "
        "Você encontra desde hambúrgueres, panquecas e até mesmo o desconhecido para nós e tradicional para eles chicken pot pie, tudo por um precinho bem bacana.",
    address: "12559 State Road 535, Orlando, FL 32836",
    site: "https://www.perkinsrestaurants.com/menu/",
  ),
  Restaurante(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando-bubba-gump.jpg",
    nome: "Bubba Gump",
    valor: "Preço Justo(US\$25 US\$40)",
    desc:
        "Uma das opções mais procuradas em Orlando para saborear uma deliciosa lagosta, ou alguns frutos do mar fresquinhos.",
    text:
        "Uma das opções mais procuradas em Orlando para saborear uma deliciosa lagosta, ou alguns frutos do mar fresquinhos. "
        "A temática foi inspirada no personagem Bubba, do filme Forest Gump, que tinha o sonho de abrir um restaurante com tudo de camarão, lembram? "
        "Enquanto você é atendido, o garçom promove um quiz bem divertido sobre o filme.",
    address: "Citywalk – 100 Universal City Plaza, Universal City",
    site: "https://www.bubbagump.com/",
  ),
  Restaurante(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando-Mels-Drive-In.jpg",
    nome: "Mel’s Drive-In",
    valor: "Barato",
    desc: "Você volta no tempo ao entrar no Mel’s Drive-In",
    text:
        "Você volta no tempo ao entrar no Mel’s Drive-In, pois além dos carros antigos estacionados à frente, "
        "todo o cenário remete às lanchonetes estadunidenses dos anos 1960, ideal para pedir aquele hambúrguer monstro, batata frita e muito milk-shake!",
    address: "Universal Studios – 6000 Universal Blvd",
    site: "http://melsdrive-in.com/",
  ),
  Restaurante(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando-via-napoli.jpg",
    nome: "Via Napoli",
    valor: "Barato(US\$15-30 por pessoa)",
    desc:
        "Apesar de ter um cardápio italiano com algumas opções, o ponto forte do Via Napoli é a pizza mesmo.",
    text:
        "Apesar de ter um cardápio italiano com algumas opções, o ponto forte do Via Napoli é a pizza mesmo. "
        "O local é perfeito para você fazer o seu pedido, sentar e comer sem cerimônia "
        "(no Epcot é necessário seguir essa dinâmica para dar tempo de ver tudo sem perder nada).",
    address: "Epcot | Pavilhão italiano – 1510 Avenue of the Stars",
    site: "https://disneyworld.disney.go.com/pt-br/dining/epcot/via-napoli/",
  ),
  Restaurante(
    img:
        "https://viagemegastronomia.com.br/wp-content/uploads/2017/06/shake-shack-02.jpg",
    nome: "Shake Shack",
    valor: "Barato(US\$10-15 por pessoa)",
    desc: " lanchonete mais frequentada de Nova Iorque chegou em Orlando ...",
    text:
        "A lanchonete mais frequentada de Nova Iorque chegou em Orlando e ganhou a sua segunda unidade no IDrive. "
        "O cardápio é elogiadíssimo por seus frequentadores e o preço é bem favorável.",
    address: "119 N Orlando Ave,Winter Park | I- Drive – 8375 International Dr",
    site: "https://www.shakeshack.com/",
  ),
  Restaurante(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando-five-guys.jpg",
    nome: "Five Guys",
    valor: "Barato(US\$10-18 por pessoa)",
    desc:
        "Lanchonete super tradicional nos Estados Unidos com hambúrgueres e acompanhamentos muito bem servidos.",
    text:
        "Lanchonete super tradicional nos Estados Unidos com hambúrgueres e acompanhamentos muito bem servidos.\n"
        "Para o carro chefe da casa, você escolhe o tamanho do hambúrguer entre normal e pequeno "
        "(lá eles chamam de ‘patty’ ou ‘little patty’) e depois ir adicionando quantos ingredientes você quiser, sem custo nenhum a mais. "
        "Eu que gosto de cheeseburguer simples, sem muita invenção. Quase sempre peço cheeseburger pequeno acebolado, com queijo, ketchup e maionese. "
        "Mas são muitos os “recheios” que você pode incluir: maionese, alface, picles, tomate, cebola grelhada, champignon grelhado, ketchup, mostarda, relish de pepino, "
        "cebolas cruas, pimenta jalapeño, pimenta verde, molho barbecue e molho apimentado. Como eu disse, o que você quiser sem custo algum.",
    address:
        "4969 International Dr Orlando, FL 32819 - Dentro do Premium Outlets da International Drive",
    site: "http://www.fiveguys.com/",
  ),
  Restaurante(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando-panera.jpg",
    nome: "Panera Bread",
    valor: "Barato(US\$5 – US\$15)",
    desc: "Pensa em uma padaria com cardápio americano. Esse é o Panera!",
    text:
        "Pensa em uma padaria com cardápio americano. Esse é o Panera! Além de ter uma infinidade de opções de cinarrols, pães salgados, doces, muffins e cookies, "
        "é possível almoçar muito bem por um preço muito baixo por lá. Bebidas não alcoólicas são vendidas por sistema de refil.",
    address: "10739 International Dr, Orlando, FL 32821, EUA",
    site: "https://www.panerabread.com/",
  ),
  Restaurante(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando-camilas.jpg",
    nome: "Camila's",
    valor: "Barato(US\$17 )",
    desc:
        "Feijoada, churrasco, guaraná, arroz com feijão e farofa… Se a saudade pela nossa comida da terrinha bater, o Camila’s com certeza será seu refúgio.",
    text:
        "Feijoada, churrasco, guaraná, arroz com feijão e farofa… Se a saudade pela nossa comida da terrinha bater, o Camila’s com certeza será seu refúgio.\n"
        "O forte do Camila’s é sua área de buffet. Acho que 99% das pessoas que comem lá vão direto no buffet, apesar deles terem um menu a la carte também. "
        "As opções de comida são sempre bem variadas. É claro que os pratos podem mudar de um dia para o outro (aos sábados por exemplo, sempre tem feijoada), "
        "mas o básico arroz, feijão, salada, farofa e carne você encontra sempre. "
        "Aliás tem muito mais opções do tipo básica que eu pelo menos encontrei sempre, como batata frita, frango grelhado, strogonoff e etc.",
    address: "5458 International Dr, Orlando, FL 32819",
    site: "https://camilasrestaurant.com/",
  ),
  Restaurante(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/06/comer-orlando-Three-Broomsticks.jpg",
    nome: "Three Broomsticks – Leaky Cauldron",
    valor: "Barato",
    desc:
        "Os dois restaurantes do Harry Potter são fast-food, mas não junk-food, ou seja, sua comida vai vir rápida, mas isso não quer dizer que ela não será saudável.",
    text:
        "Os dois restaurantes do Harry Potter são fast-food, mas não junk-food, ou seja, sua comida vai vir rápida, mas isso não quer dizer que ela não será saudável. "
        "Há diversas opções no cardápio, este que está disponível em uma vitrine na fila do caixa. Além das opções temáticas, "
        "há também alguns pratos de origem inglesa, como a tradicional Fish & Chips. Ah, não esqueça de experimentar a cerveja amanteigada, hein? "
        "(e dê preferencia para a cerveja frozen).",
    address: "Islands of Adventure – Universal Studios",
    site: "https://orlandoinformer.com/universal/three-broomsticks-hogs-head",
  ),
];
