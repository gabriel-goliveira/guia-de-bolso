class Passeio {
  ///Image link to get online image
  final String img;

  ///Tour name
  final String name;

  ///Tour description
  final String desc;

  ///Tour post content
  final String text;

  ///Tour address
  final String address;

  ///Tour weblink
  final String site;

  const Passeio({
    this.img,
    this.name,
    this.desc,
    this.text,
    this.address,
    this.site,
  });
}

///Tour object list
final passeios = [
  new Passeio(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/04/orlando-fora-dos-parques-15-730x486.jpg",
    name: "Orlando Brewing Company",
    desc:
        "Existem mais de 20 rótulos disponíveis na Orlando Brewing Company, a microcervejaria totalmente orgânica de Orlando.",
    text:
        "Existem mais de 20 rótulos disponíveis na Orlando Brewing Company, a microcervejaria totalmente orgânica de Orlando. "
        "O lugar é um prato cheio para quem gosta de cervejas artesanais e quer experimentar as maravilhas do lúpulo fresco norte-americano. "
        "A fábrica oferece tours gratuitos na cervejaria de segunda a sexta às 18h e conta com música ao vivo nas sextas e sábados, a partir das 19h.",
    address: "1301 Atlanta Ave, Orlando, FL 32806, EUA",
    site: "http://www.orlandobrewing.com/",
  ),
  Passeio(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/04/orlando-fora-dos-parques-04_2-730x481.png",
    name: "Icebar",
    desc:
        "Pertinho do Orlando Eye existe a diversão perfeita para os mais crescidinhos: o Icebar",
    text:
        "Pertinho do Orlando Eye existe a diversão perfeita para os mais crescidinhos: o Icebar segue o modelo tradicional dos bares de gelo espalhados pelo mundo. "
        "Aproveite para se refrescar e curtir boas bebidas – lembrando que, nos Estados Unidos, a idade mínima para o consumo de álcool é 21 anos.",
    address: "8967 International Dr, Orlando, FL 32819, EUA",
    site: "https://icebarorlando.com/",
  ),
  Passeio(
    img:
        "https://www.viajali.com.br/wp-content/uploads/2016/04/orlando-fora-dos-parques-03-730x456.jpg",
    name: "OrlandoEye",
    desc:
        "De cima da Orlando Eye, você terá a belíssima vista panorâmica do centro da Flórida.",
    text:
        "O recém-inaugurado complexo de entretenimento é uma ótima opção para quem quer ficar longe do Mickey. "
        "De cima da Orlando Eye, você terá a belíssima vista panorâmica do centro da Flórida.",
    address: "8375 International Dr, Orlando, FL 32819, EUA",
    site: "https://iconparkorlando.com/",
  ),
];
