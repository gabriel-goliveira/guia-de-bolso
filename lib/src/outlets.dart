class Outlets {
  ///Image link to get online image
  final String img;

  ///Outlet name
  final String name;

  ///Category. Tipically Outlet or Shopping
  final String category;

  ///Outlet description
  final String desc;

  ///Outlet post content
  final String text;

  ///Outlet map image link
  final String map;

  ///Outlet address
  final String address;

  ///Outlet opening hours
  final String hour;

  ///Outlet rating
  final double rating;

  ///Outlet weblink
  final String site;

  const Outlets({
    this.img,
    this.name,
    this.category,
    this.desc,
    this.text,
    this.map,
    this.address,
    this.hour,
    this.rating,
    this.site,
  });
}

///Outlets object list
final outlets = [
  new Outlets(
    img:
        "https://adc3ef35f321fe6e725a-fb8aac3b3bf42afe824f73b606f0aa4c.ssl.cf1.rackcdn.com/propertyimages/1332/orlando-premium-outlets-international-drive-04.jpg",
    name: "Orlando Premium Outlets – International Drive",
    category: "Outlet",
    desc:
        "Este é um Outlet da rede Premium, o que significa que é de qualidade e possui grandes marcas.",
    text:
        "Este é um Outlet da rede Premium, o que significa que é de qualidade e possui grandes marcas. "
        "Ele é todo aberto, o que pode ser um incômodo no verão ou no inverno, mas sempre tem ar condicionado dentro das lojas, então dá para aguentar bem! \n"
        "São mais de 180 lojas, e as minhas favoritas são: Lacoste, Michael Kors, Polo Ralph Lauren, Victoria’s Secret, Adidas, Banana Republic Factory Store, "
        "Calvin Klein, DKNY, Forever 21, Gap Outlet, Guess Factory Store, Nike Factory Store, U.S. Polo Assn., Tommy Hilfiger, Disney’s Character Warehouse, "
        "Crocs, Kate Spade New York e Bath & Body Works.\n"
        "Lá possui uma praça de alimentação bem grande, onde você encontrará: Five Guys, Haagen Dazs, Nestle Toll House, "
        "Pinkberry, Sbarro, Starbucks Coffee, Subway, Tropical Sensations, Vinito Ristorante etc. \n"
        "Dica importante: Cadastre-se no site oficial do Premium Outlets e imprima cupons de desconto exclusivos! "
        "Além dos cupons você também consegue imprimir um voucher para retirar de graça o livro de descontos no Information Center do Outlet. "
        "E os cupons que você imprime no site são diferentes dos que constam no livro que você retira no shopping. Então deixe para imprimir os cupons em uma "
        "data próxima a da sua viagem, pois muitos deles tem data de expiração curta.",
    map:
        "https://i0.wp.com/www.viajaterapia.com/wp-content/uploads/2018/07/international.jpg?resize=1024%2C574",
    address: "4951 International Dr, Orlando, FL 32819, Estados Unidos",
    hour:
        "De Segunda-feira à Sábado: das 10 horas às 23 horas / Domingo: das 10 horas às 21 horas.",
    rating: 5,
    site: "https://www.premiumoutlets.com/oops",
  ),
  new Outlets(
    img:
        "https://andrezadicaeindica.com.br/wp-content/uploads/2015/11/florida-mall-1280x720.jpg",
    name: "The Florida Mall",
    category: "Shopping",
    desc:
        "É o shopping mais completo de Orlando. Foi fundado em 1986 e possui uma variedade bem grande de lojas!",
    text:
        "É o shopping mais completo de Orlando. Foi fundado em 1986 e possui uma variedade bem grande de lojas! \n"
        "São mais de 250 lojas com excelentes preços e muita variedade, e mesmo não sendo um outlet, você encontrará ótimos preços! "
        "As minhas lojas favoritas por lá são: Nordstrom, LUSH, Barnes & Noble, The Body Shop, Best Buy, Forever 21, Macy’s, Sephora, Zara, H&M, M&M’s World, Claire’s, "
        "Victoria’s Secret, The Disney Store, Bath & Body Works, Game Stop, Nordstrom, Saks, Sears, Swatch e Dylan’s Candy Bar e Ross. "
        "Do outro lado da rua você também pode conferir uma gigantesca Toys ‘R’ Us e uma Best Buy. \n"
        "Ele é todo fechado, iguais aos shoppings do Brasil, ou seja, não importa se é verão ou inverno, você não vai sofrer com a temperatura.\n"
        "PS. Um boa dica é estacionar o carro perto de uma grande loja e entrar por ela no shopping, assim na hora de ir embora você já sabe onde seu carro está. É uma ótima referencia!",
    map: "https://pt.maps-orlando.com/img/1200/florida-mall-mapa.jpg",
    address: "8001 S Orange Blossom Trail, Orlando, FL 32809, Estados Unidos",
    hour:
        "De Segunda a Sábado: das 10:00 horas até 22:00 horas / Domingos: das 12:00 horas até às 20:00 horas",
    rating: 5,
    site: "https://www.simon.com/mall/the-florida-mall/map#/",
  ),
  new Outlets(
    img:
        "http://parquesdeorlando.com.br/wp/wp-content/uploads/2017/09/orlando-premium-outlets-08-1000x640.jpg",
    name: "Orlando Premium Outlets – Vineland Ave",
    category: "Outlet",
    desc:
        "É mais um Outlet em Orlando da rede Premium, ou seja, tem a mesma qualidade do International Drive",
    text:
        "É mais um Outlet em Orlando da rede Premium, ou seja, tem a mesma qualidade do International Drive e também é todo aberto, "
        "mas acho que vale a pena conhecer os dois, já que aqui tem lojas diferentes!\n"
        "São dois os outlets da rede em Orlando, com o mesmo nome. Um está localizado na Vineland Ave e o outro na International Drive. Este texto refere-se apenas ao da Vineland Ave.\n"
        "São mais de 150 lojas disponíveis, e minhas favoritas são: Burberry, Gap Outlet, Lacoste, Nike, Polo Ralph Lauren, Tommy Hilfiger, "
        "CH Carolina Herrera, Guess Factory Store, Michael Kors, Prada, Disney’s Character Premiere, Adidas, ALDO, Crocs, Vans, Kipling, Swarovski, Perfumania e The Cosmetics Company\n"
        "As opções da praça de alimentação são: A&W/Old Town Ice Cream, Auntie Anne’s Soft Pretzels, Chicken Now, China Max, Frozeberry, "
        "The Fudgery, Maki of Japan, Mrs. Fields, Starbucks Coffee, Subway, Sundial Café, Taco Bell e Villa Fresh Italian Kitchen.\n"
        "Dica importante: Não deixe se cadastrar no site oficial do Premium Outlet e imprimir cupons de desconto exclusivos! "
        "Além dos cupons você também consegue imprimir um voucher para retirar de graça o livro de descontos no Information Center do Outlet. "
        "E os cupons que você imprime no site são diferentes dos que constam no livro que você retira no shopping. "
        "Então deixe para imprimir os cupons em uma data próxima a da sua viagem, pois muitos deles tem data de expiração curta.\n"
        "Ah, se estiver viajando com mais gente, aproveite para juntar as compras e utilizar o cupom juntos. "
        "Os cupons do site normalmente oferecem descontos maiores do que os do livo, mas tem um valor mínimo para a compra. "
        "Preste atenção para poder aproveitar mais!",
    map:
        "https://i2.wp.com/www.viajaterapia.com/wp-content/uploads/2018/07/vineland.jpg?resize=1024%2C574",
    address: "8200 Vineland Ave, Orlando, FL 32821, Estados Unidos",
    hour:
        "De Segunda a Sábado: das 10:00 horas até 23:00 horas / Domingos: das 10:00 horas até às 21:00 horas",
    rating: 4.5,
    site: "https://www.premiumoutlets.com/oops",
  ),
  new Outlets(
    img:
        "https://i1.wp.com/www.julyanelima.com/blog/wp-content/uploads/2014/10/mall-millenia.jpg?w=670&ssl=1",
    name: "Mall at Millenia",
    category: "Shopping",
    desc:
        "Este shopping é sem dúvidas o mais luxuoso de Orlando, foi inaugurado em 2002 e possui diversas lojas de grifes famosas, porém também possui lojas econômicas.",
    text:
        "Este shopping é sem dúvidas o mais luxuoso de Orlando, foi inaugurado em 2002 e possui diversas lojas de grifes famosas, porém também possui lojas econômicas.\n"
        "Minhas lojas favoritas são: Apple Store, Forever 21, GAP, L’Occitane, MAC, Bath & Body Works, Sanrio, Juicy Couture, Betsey Johnson, "
        "Hollister Co, Zara, Urban Outfitters, Victoria’s Secret, Sanrio, Macy’s, Abercrombie, Tiffany & Co e Bloomingdale’s.\n"
        "O shopping é bem grande, por isso recomendo que comece pelo andar de baixo e depois de termina-lo, vá para o andar de cima. "
        "Uma boa dica para não esquecer onde deixou seu carro é estacionar perto da entrada principal. \n"
        "Possui uma grande praça de alimentação ma também conta com restaurantes, como o famoso The Cheesecake Factory! Muito conhecido pelos seus cheesecakes de inúmeros sabores.",
    map: "https://www.falandodeviagem.com.br/mallatmilleniamap01.jpg",
    address: "4200 Conroy Rd, Orlando, FL 32839, Estados Unidos",
    hour:
        "De Segunda a Sábado: das 10:00 horas até 21:00 horas / Domingos: das 11:00 horas até às 19:00 horas",
    rating: 5,
    site: "https://www.mallatmillenia.com/",
  ),
];
