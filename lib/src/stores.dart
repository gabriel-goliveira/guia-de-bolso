class Store {
  ///Image link to get online image
  final String img;

  ///Store name
  final String name;

  ///Store category
  final String category;

  ///Store description
  final String desc;

  ///Store post content
  final String text;

  ///Store address
  final String address;

  ///Store opening hours
  final String hour;

  ///Store rating
  final double rating;

  ///Store weblink
  final String site;

  const Store({
    this.img,
    this.name,
    this.category,
    this.desc,
    this.text,
    this.address,
    this.hour,
    this.rating,
    this.site,
  });
}

///Store object list
final stores = [
  new Store(
    img:
        "https://dicasdadisneyeorlando.com.br/wp-content/uploads/2015/10/supermercado-Walmart-em-orlando.jpg",
    name: "Walmart",
    category: "Mercado",
    desc:
        "Se você nunca foi no Walmart dos EUA, provavelmente você não está entendendo porque um mercado entrou na lista… Mas é um paraíso, sério!"
        "Nem se compara com o Walmart do Brasil, esqueça dele pois aqui nos EUA você vai encontrar de tudo, e por um preço muito bom.",
    text:
        "Se você nunca foi no Walmart dos EUA, provavelmente você não está entendendo porque um mercado entrou na lista… Mas é um paraíso, sério!"
        " Nem se compara com o Walmart do Brasil, esqueça dele pois aqui nos EUA você vai encontrar de tudo, e por um preço muito bom. "
        "Visitei, aliás, um de Nova York, mas em Orlando além de ser mais barato, possui vários produtos da Disney. \n"
        "Recomendo muito passar em Walmart ou Target (não listei ele aqui pois ainda não conheci) logo no início da viagem para economizar ainda mais. "
        "Vou listar alguma das coisas que achei que mais valeram a pena: \n\n"
        "- Lanchinhos para levar no parque e para tomar café da manhã no quarto do hotel; \n\n"
        "- Protetor solar, lencinhos umedecidos e álcool gel. Recomendo fazer estoque, pois é muito barato! \n\n"
        "- Roupas da Disney, pois são licenciadas e muito mais baratas do que nos parques; \n\n"
        "- Shampoo, condicionador, esmaltes e todos os produtos de beleza que você quiser comprar, é sempre mais barato se tiver no Walmart; \n\n"
        "- Malas! Compramos 3 malas grande aqui, 2 de qualidade inferior por \$25 e outra que é muito boa por \$40 \n",
    address: "3250 Vineland Rd, Kissimmee, FL 34746, Estados Unidos",
    hour: "24h",
    rating: 5,
    site: "https://www.walmart.com/",
  ),
  new Store(
    img:
        "https://cache.quantocustaviajar.com/blog/wp-content/uploads/2018/07/fazer-compras-em-orlando-1.jpg",
    name: "Ross, Dress for Less",
    category: "Roupas",
    desc:
        "Como o próprio slogan da loja diz: “Ross, dress for less” que em português significa algo como “Ross, vista-se por menos”. E é basicamente isso! "
        "Aqui é revendida peças de coleções antigas das marcas famosas como: Nike, Adidas, Tommy Hilfiger, Ralph Lauren, Levi’s etc.",
    text:
        "Como o próprio slogan da loja diz: “Ross, dress for less” que em português significa algo como “Ross, vista-se por menos”. E é basicamente isso! "
        "Aqui é revendida peças de coleções antigas das marcas famosas como: Nike, Adidas, Tommy Hilfiger, Ralph Lauren, Levi’s etc. "
        "E por ser coleções antigas, tem saldão todos os dias. Tem calça jeans por menos de \$7! \n"
        "Mas graças a esses preços baixos, é bem difícil de conseguir fazer boas compras por lá pois a procura é grande e bagunça da loja também! "
        "Vá preparado para passar um bom tempo por lá para olhar tudo e verificar cada peça, pois algumas possui algum defeito e até qualidade duvidosa.\n"
        "Por lá também é possível encontrar produtos para casa, como: roupas de cama, toalhas e artigos de decoração. "
        "E se você está pensando em comprar mala, aqui também é um bom lugar pois é possível encontrar malas de marcas conhecidas como a Samsonite.\n"
        "Dicas para aproveitar melhor a Ross:\n\n"
        "- Procure ir em dias de semana (de Terça a Sexta), pois aos finais de semana as lojas ficam mais cheias. "
        "Também prefira ir logo de manhã, quando as mercadorias estão mais organizadas e fica mais fácil encontrar o que estamos procurando;\n\n"
        "- Experimente tudo! Como algumas peças de roupa podem ter pequenos defeitos é sempre bom olhar tudo antes de levar!\n\n"
        "- Se possível, não vá na Ross no mesmo dia que pretenda visitar um parque pois não vai dar para ver tudo!\n",
    address: "2756 E Colonial Dr, Orlando, FL 32803, Estados Unidos",
    hour:
        "De Segunda a Sábado: das 09:30 horas até 21:30 horas / Domingo: das 11:00 horas até às 19:00 horas",
    rating: 4.5,
    site: "https://www.rossstores.com/",
  ),
  new Store(
    img:
        "https://dicasdadisneyeorlando.com.br/wp-content/uploads/2016/08/TJ-Maxx-Store-international-drive-orlando-value-center-1024x681.jpg",
    name: "T.J. Maxx",
    category: "Varejo",
    desc:
        "A T.J. Maxx é uma grande loja de varejo que está presente em todos os Estados Unidos. É uma loja onde você encontrará de tudo um pouco, com variedade e ótimos preços.",
    text:
        "A T.J. Maxx é uma grande loja de varejo que está presente em todos os Estados Unidos. É uma loja onde você encontrará de tudo um pouco, com variedade e ótimos preços.\n"
        "Conhecida como TJ’S pelos americanos, seu forte são as roupas e acessórios, mas você encontrará muitos artigos para casa, "
        "malas, produtos de beleza, mochilas, bolsas, artigos de informática, perfumes etc. A loja funciona de uma forma bem legal, ela basicamente negocia por um "
        "ótimo preço o estoque que sobrou de outras lojas de departamento maiores, e que só compram coleções inteiras e da última estação. "
        "Por isso encontramos peças de designers como Prada, Gucci, Dolce & Gabbana, Juicy Couture etc.\n"
        "E por estar sempre comprando estoques de outras lojas, sempre tem novidade por lá, por isso tente chegar cedo na loja para encontrar os melhores produtos.",
    address: "80 W Grant St, Orlando, FL 32806, Estados Unidos",
    hour:
        "De Segunda a Quinta: das 09:30 horas até 22:30 horas / Sexta a Sábado: das 09:30 horas até 23:00 horas / Domingo: das 11:00 horas até às 21:00 horas",
    rating: 5,
    site:
        "https://tjmaxx.tjx.com/store/stores/Orlando-FL-32832/1499/aboutstore",
  ),
  new Store(
    img:
        "https://dicasdadisneyeorlando.com.br/wp-content/uploads/2018/12/loja-marshalls-em-orlando.jpg",
    name: "Marshalls",
    category: "Roupas",
    desc:
        "A Marshalls é bem parecida com a Ross, mas é um pouco maior que sua concorrente, além de ser mais organizada!",
    text:
        "A Marshalls é bem parecida com a Ross, mas é um pouco maior que sua concorrente, além de ser mais organizada!"
        "Também é possível encontrar bolsas e roupas de grifes famosas como BCBGirls, Roxy, Guess, Free People, DKNY, Michael Kors etc.\n"
        "Foi aqui que encontrei diversos casacos baratos, além de muitos perfumes famosos por \$20/\$30. Chegue cedo e você vai encontrar muita coisa!",
    address: "730 Sand Lake Rd, Orlando, FL 32809, Estados Unidos",
    hour:
        "De Segunda a Quinta: das 09:30 horas até 21:30 horas / Sexta a Sábado: das 09:30 horas até 22:00 horas/ Domingo: das 10:00 horas até às 20:00 horas",
    rating: 4.5,
    site: "https://www.marshalls.com/",
  ),
  new Store(
    img:
        "https://www.disneydenovo.com/wp-content/uploads/2016/02/Dollar_Tree_Orlando.jpg?x23774",
    name: "Dollar Tree",
    category: "Lembrancinhas",
    desc:
        "Dollar Tree se diferencia das demais lojas porque lá dentro TUDO custa um dólar. "
        "Esta rede está presente nos Estados Unidos inteiro e também no Canadá... ",
    text:
        "Dollar Tree se diferencia das demais lojas porque lá dentro TUDO custa um dólar. "
        "Esta rede está presente nos Estados Unidos inteiro e também no Canadá e em nenhuma de suas lojas você encontra "
        "etiquetas de preços nas prateleiras e corredores, pois todos os itens custam sempre a mesma coisa: 1 dólar.\n"
        "Na Dollar Tree você economiza em compras menores que você normalmente nem planejou mas vai acabar gastando quando chegar em Orlando. "
        "Muitas vezes são itens que você nem vai levar pro Brasil de volta.",
    address: "5295 International Dr #400, Orlando, FL 32819, EUA",
    hour:
        "De Segunda a Sábado: das 09:00 horas até 21:00 horas / Domingos: das 10:00 horas até às 19:00 horas",
    rating: 5,
    site: "https://www.dollartree.com/bulk/Orlando",
  ),
  new Store(
    img:
        "https://cache.quantocustaviajar.com/blog/wp-content/uploads/2018/07/fazer-compras-em-orlando-2-1.jpg",
    name: "Bed Bath & Beyond",
    category: "Itens para casa",
    desc:
        "Se você está em busca de itens para sua casa, vá até uma das lojas da Bed Bath & Beyond. ",
    text:
        "Se você está em busca de itens para sua casa, vá até uma das lojas da Bed Bath & Beyond. "
        "Artigos de roupa, mesa, cama, eletrônicos, jardinagem e mais uma infinidade de itens você encontra aqui.\n"
        "O legal dessa loja é que os produtos e utensílios, além de terem bons preços, também são encantadores. "
        "As toalhas e jogos de cama possuem qualidade acima da média, os itens de decoração são de deixar qualquer pessoa apaixonada!\n"
        "O difícil é visitar a Bed Bath & Beyond e não ficar com vontade de trazer uma churrasqueira para o Brasil! É sério: tem muita coisa linda, útil e com bom preço.",
    address: "5295 International Dr Ste 100, Orlando, FL 32819, EUA",
    hour:
        "De Segunda a Sábado: das 09:30 horas até 21:30 horas / Domingos: das 10:00 horas até às 20:30 horas",
    rating: 5,
    site: "https://www.bedbathandbeyond.com/",
  ),
  new Store(
    img:
        "https://cache.quantocustaviajar.com/blog/wp-content/uploads/2018/07/fazer-compras-em-orlando-3.jpg",
    name: "IKEA",
    category: "Itens para casa",
    desc:
        "A IKEA é mais um paraíso para quem deseja fazer compras em Orlando e está em busca de itens para decorar a casa, gastando pouco.",
    text:
        "A IKEA é mais um paraíso para quem deseja fazer compras em Orlando e está em busca de itens para decorar a casa, gastando pouco. "
        "A loja possui dois andares com uma infinidade de itens de decoração, jogos de jantar, talheres, panelas, copos, móveis e muito mais!\n"
        "Até luminárias são vendidas na loja, bem como edredons, almofadas e itens para pets — "
        "e com certeza são itens que você não vai encontrar nem parecidos aqui no Brasil (ou se encontrar, serão “uma facada”).",
    address: "4092 Eastgate Dr, Orlando, FL 32839, EUA",
    hour:
        "De Segunda a Sábado: das 10:00 horas até 20:00 horas / Domingos: das 10:00 horas até às 20:00 horas",
    rating: 4.5,
    site: "https://www.ikea.com/us/en/store/orlando",
  ),
  new Store(
    img:
        "https://cache.quantocustaviajar.com/blog/wp-content/uploads/2018/07/fazer-compras-em-orlando-4.jpg",
    name: "Ulta Beauty",
    category: "Cosméticos",
    desc:
        "A Ulta Beauty é o paraíso das maquiagens e produtos de beleza e quem deseja fazer compras em Orlando sem gastar muito deve inclui-la na lista. ",
    text:
        "A Ulta Beauty é o paraíso das maquiagens e produtos de beleza e quem deseja fazer compras em Orlando sem gastar muito deve inclui-la na lista."
        "Acessórios como secadores, escovas, chapinhas e babyliss também são encontrados aqui. \n"
        "Em se tratando de maquiagens, você não vai encontrar produtos da MAC, por exemplo, na Ulta. "
        "Mas marcas mais simples e de muita qualidade, como Maybelline, Revlon e Nyx, estarão nas prateleiras. "
        "Produtos da Urban Decay, Smashbox, Too Faced e Benefit também são encontrados com precinhos camaradas. \n"
        "Para tratamentos para os cabelos, você pode comprar produtos de marcas como Schwarzkopf, L’oréal, Redken, Joico e muito mais.",
    address: "4048 Eastgate Dr, Orlando, FL 32839, EUA",
    hour:
        "De Segunda a Sábado: das 10:00 horas até 21:00 horas / Domingos: das 11:00 horas até às 18:00 horas",
    rating: 4.2,
    site: "https://www.ulta.com",
  ),
  new Store(
    img: "https://s3-media4.fl.yelpcdn.com/bphoto/f4e7c32ichxyt8HuVzJ3nA/o.jpg",
    name: "Perfumeland",
    category: "Cosméticos",
    desc:
        "A Perfumeland é uma loja imensa focada na venda de perfumes e cosméticos.",
    text:
        "A Perfumeland é uma loja imensa focada na venda de perfumes e cosméticos. "
        "As marcas mais famosas e cobiçadas de perfumes, como Burberry, Calvin Klein, Carolina Herrera e Dior são encontradas aqui. \n"
        "Já as linhas de cosméticos possuem itens para tratamento dos cabelos que atendem até os profissionais,"
        " com máscaras, shampoos e condicionadores de marcas famosas como: L’oréal, Schwarzkopf, Kérastase entre outras. \n"
        "Muitos atendentes (assim como na maioria das lojas de shoppings e outlets na cidade) "
        "são brasileiros e isso pode facilitar a vida de quem vai fazer compras em Orlando, caso você não esteja segura em se comunicar 100% em inglês "
        "e precise de mais explicações sobre os tratamentos.",
    address: "5161 International Dr, Orlando, FL 32819, EUA",
    hour: "10:00 - 22:00",
    rating: 5,
    site: "https://megastore.perfumeland.com/",
  ),
  new Store(
    img:
        "https://cache.quantocustaviajar.com/blog/wp-content/uploads/2018/07/fazer-compras-em-orlando-7.jpg",
    name: "Target",
    category: "Mercado",
    desc:
        "Mais um supermercado que você deve conhecer se deseja fazer compras em Orlando.",
    text:
        "Mais um supermercado que você deve conhecer se deseja fazer compras em Orlando. "
        "A variedade de produtos é imensa e, assim como no Walmart, você não vai sair com a sacola cheia apenas de alimentos!\n"
        "Na Target, você vai encontrar itens de presente (os souvenires da Disneys com preços ótimos), itens para a casa, "
        "que englobam desde eletrodomésticos a copos e talheres, seção com produtos para cama, mesa e banho, itens de farmácia e muito mais!\n"
        "Se comparada com o Walmart, a Target oferece produtos com maior qualidade, principalmente os alimentos. "
        "No Walmart, os produtos industrializados são mais em conta, isso é verdade, mas se você quer algo fresco "
        "para preparar um jantar rápido em casa, vale conferir a seção de frutas e verduras.",
    address: "4750 Millenia Plaza Way, Orlando, FL 32839, EUA",
    hour: "24hs",
    rating: 5,
    site: "https://www.target.com/",
  ),
  new Store(
    img:
        "https://www.vaipradisney.com/blog/wp-content/uploads/2015/01/best-buy-orlando-millenia-shopping-560x374.jpg",
    name: "Best Buy",
    category: "Eletrônicos",
    desc:
        "A Best Buy é talvez, a loja mais frequentada pelos Brasileiros que viajam para os Estados Unidos, principalmente por aqueles que vão para Orlando.",
    text:
        "Lojas grandes de eletrônicos como a Best Buy não se limitam a vender os eletrônicos como computadores e tablets, "
        "mas também eletro-eletrônicos para casa, DVDs, sofás, balanças, jogos de video game e até mesmo escova de dente elétrica! "
        "Qualquer coisa que você pode vir a ligar na tomada (ou não!) você deve encontrar na Best Buy. \n"
        "A fórmula é imbatível: com muita variedade de produtos, bons preços e tudo no mesmo lugar, "
        "a Best Buy acaba arrecadando milhões de dólares pois facilita a nossa vida durante as compras da viagem. "
        "Na região de Orlando, são 10 lojas que ficam forradas de turistas, sendo que quatro dessas ficam em cidades próximas, de até meia hora de distância do centro.",
    address: "4601 East Colonial Drive, Orlando",
    hour:
        "De Segunda a Sábado: das 09:30 horas até 21:30 horas / Domingos: das 10:00 horas até às 20:30 horas",
    rating: 5,
    site: "https://www.bestbuy.com/",
  ),
  new Store(
    img: "https://s3-media2.fl.yelpcdn.com/bphoto/M_v8eIuruXHGmx8z7CiPZg/o.jpg",
    name: "Remix Record Shop",
    category: "Música",
    desc: "Maior loja de discos de Orlando!",
    text:
        "Maior loja de discos de Orlando! Conta com uma enorme variedade de discos e cd's",
    address: "1213 N Mills Ave, Orlando, FL 32803, EUA",
    hour:
        "De Segunda a Sábado: das 09:30 horas até 21:30 horas / Domingos: das 10:00 horas até às 20:30 horas",
    rating: 4,
    site: "https://remixrecordshop.com/",
  ),
  new Store(
    img:
        "https://dicasdadisneyeorlando.com.br/wp-content/uploads/2018/12/loja-five-bellow-orlando.jpg",
    name: "Five Below",
    category: "Lembrancinhas",
    desc:
        "A Five Below está entre as lojas imperdíveis em Orlando para comprar coisas mais simples com um valor máximo de 5 dólares",
    text:
        "A Five Below está entre as lojas imperdíveis em Orlando para comprar coisas mais simples, como brinquedos, acessórios de beleza, artigos para festa de aniversário, "
        "itens de papelaria, doces, capinhas para celular e até carregadores. Ela é do mesmo estilo da loja Dollar Tree, mas com um valor máximo de 5 dólares e mais focada "
        "em crianças e adolescentes. É muita variedade de produtos e tem muita coisa legal na Five Bellow.",
    address: "International Drive Value Center",
    hour: "9:00 - 21:00",
    rating: 5,
    site: "https://www.fivebelow.com/",
  )
];
