class Informations {
  ///Image link to get online image
  final String img;

  ///Info title
  final String titulo;

  ///Information description
  final String desc;

  ///Post content
  final String text;

  Informations({this.img, this.titulo, this.desc, this.text});
}

///Informations object list
final informations = [
  Informations(
    img:
        "https://www.vaipradisney.com/blog/wp-content/uploads/2015/08/22865773483_1e9f7d19e7_k-780x520.jpg",
    titulo: "Dicas para quem não fala inglês",
    desc: "Como se virar durante a viagem, sem saber falar inglês?",
    text:
        "A primeira viagem internacional é sempre um marco importante na vida de uma pessoa, mas junto com ela, "
        "vem uma série de inseguranças que são muitas vezes ligadas ao idioma desconhecido. Como se virar durante a viagem, sem saber falar inglês?\n"
        "Sempre digo que em Orlando essa preocupação não é necessária, já que a cidade está mais do que acostumada a receber turistas brasileiros há muitos anos. "
        "Por isso, não estranhe se você mais ouvir (e também falar) português do que inglês por ali. Além disso, como toda a Flórida é tomada pelos latinos, "
        "quando você não ouvir português, boas são as chances de você ouvir espanhol, o que facilita a comunicação também.\n"
        "Aqui estão algumas frases para ajudar na comunicação:\n"
        "- I’m from Brazil, I don’t speak english.(lê-se 'Ai em from Brasil, Ai donti espique inglexi')\n"
        "Tradução: 'Eu sou do Brasil e não falo inglês'\n\n"
        "- Excuse me\n"
        "Tradução: 'Licença, desculpe-me'\n\n"
        "- Where is the restroom?(uere is de restirum?)\n"
        "Tradução: 'Onde fica o banheiro?'\n\n"
        "- Please(pliss)\n"
        "Tradução: 'Por favor'\n\n"
        "Thank you.\n"
        "Tradução: 'Obrigado'\n\n"
        "Frases Importantes na imigração:\n\n"
        "- Pergunta do oficial: How many days are you planning to stay in the United States?\n"
        "Tradução: Quantos dias você planeja ficar nos EUA?\n"
        "Exemplo Resposta: Ten days (10 dias) ou 2 weeks (duas semanas).\n\n"
        "- Pergunta do oficial: What do you do for a living?\n"
        "Tradução: O que você faz da vida?\n"
        "Exemplo de resposta: I am businessman (eu sou empresário) ou I study (eu estudo).\n\n"
        "- Pergunta do oficial: Where are you going in the United States?\n"
        "Tradução: Aonde você vai nos EUA?\n"
        "Exemplo de resposta: I will stay in Orlando (eu vou ficar em Orlando)\n\n"
        "- Pergunta do oficial: Where are you going in the United States?\n"
        "Tradução: Aonde você vai nos EUA?\n"
        "Exemplo de resposta: I will stay in Orlando (eu vou ficar em Orlando)\n\n"
        "- Pergunta do oficial: How much money in cash are you bringing with you?\n"
        "Tradução: Quando de dinheiro em espécie você está trazendo?\n"
        "I have tree thousand dollars in cash but I’m also bringing two credit cards and one travel card with five hundred dollars in it "
        "(eu tenho 3 mil dólares em espécie, mas também estou trazendo 2 cartões de crédito e 1 cartão de viagem com 500 dólares de crédito). "
        "Lembre-se que cada família deve declarar o valor caso esteja entrando nos EUA com mais de \$10 mil dólares (para toda a família).\n\n"
        "Frases Importantes Hotel:\n\n"
        "- Você diz em inglês: Hello, I’m checking in today. I have a reservation on my name, Gabriel Gomes.\n"
        "O que quer dizer: Oi. Estou fazendo check in hoje. Tenho uma reserva no meu nome, Gabriel Gomes\n\n"
        "- O funcionário do hotel provavelmente vai responder algo como: I need a photo ID and a credit card to check you in.\n"
        "O que quer dizer: Eu preciso de um documento com foto (passaporte!) e um cartão de crédito para fazer o seu check in."
        "Sobre a entrega do cartão, isso é uma política comum e em geral nada é cobrado a não ser que você consuma alguma coisa.\n\n"
        "- O funcionário do hotel pergunta: How many nights are you staying with us?\n"
        "O que quer dizer: Quantas noites você irá se hospedar conosco?\n\n"
        "- O funcionário do hotel pergunta: Could you please fill in this form?\n"
        "O que quer dizer: Você poderia preencher esse formulário?\n\n"
        "Palavras Importantes parque:\n\n"
        "Turnstiles = catracas\n"
        "Fireworks = show de fogos\n"
        "Parade = paradas / desfiles\n"
        "Roller coaster = montanha russa\n"
        "Minimum height = altura mínima\n"
        "Attraction = atração\n"
        "Stand by (fila) = fila normal para entrar na atração (não é Fastpass+ ou Express)\n\n"
        "Palavras Importantes restaurantes:\n\n"
        "Reservation = reserva \n"
        "Check = conta. Se você falar “bill” eles também vão entender que é a conta, mas check é a forma mais educada. Basta dizer 'the check, please'\n"
        "Tip ou gratuity = gorjeta.\n"
        "Change = troco.\n"
        "Buffet = igual aos buffets do Brasil. Há uma ilha de comida onde você pode se servir do que quiser, quantas vezes quiser.\n"
        "Table service = restaurante a la carte, com serviço de mesa, garçon\n"
        "Waiter/Waitress = garçom/garçonete.\n\n"
        "Dica: Nos EUA, você quase sempre deve esperar na porta do restaurante uma pessoa vir te receber e te levar até a mesa. "
        "Mesmo que ela demore um pouco e o restaurante esteja vazio. Dai é só mandar um 'table for two' "
        "(ou o número de pessoas que tiver no seu grupo) que o host ou a hostess vai te levar até uma mesa do seu tamanho.\n\n"
        "O pedido sempre começa com “I would like to order a….” e dai é só falar o nome do prato.\n\n"
        "Frases Importantes lojas:\n\n"
        "- Cupons = cupons de desconto. Se fala praticamente igual em português \n"
        "- How much is this? = quanto custa isso?\n"
        "- Do you have this in size __? = Você tem isso no tamanho __? Aí lógico, você completa com o tamanho que quer.\n"
        "- Do you have this in blue? = Você tem isso em azul? (ou outra cor que você queira).\n"
        "- Where is the fitting room? = Onde fica o provador?\n\n",
  ),
  Informations(
      img:
          "https://www.vaipradisney.com/blog/wp-content/uploads/2015/01/arrumar-a-mala-viagem-eua-orlando-04-560x420.jpg",
      titulo: "Dicas Mala de Mão",
      desc: "O que levar na mala de mão?",
      text:
          "Essa mala deve ser pequena para caber no bagageiro ou embaixo do assento a sua frente no avião. "
          "Algumas cias aéreas ainda pesam essa bagagem para garantir que ela esteja dentro do peso e do tamanho permitido, por isso, "
          "não deixe de consultar quais são as restrições da empresa que você está viajando.\n\n"
          "O que levar na mala de mão?\n\n"
          "- Documentos de viagens: passaporte com o visto, reservas dos hotéis/carro, vouchers dos parques impressos.\n"
          "- Um casaco, já que faz bastante frio no avião.\n"
          "- Uma troca de roupa, para caso a minha mala despachada seja extraviada ao chegar no destino.\n"
          "- Carregadores em geral, principalmente um para o celular.\n"
          "- Uma pequena necessaire: com escova/pasta de dente, um creme de mão e uma manteiga de cacau já que dentro do avião o ar é bem seco.\n"),
  Informations(
      img:
          "http://www.vaipradisney.com/blog/wp-content/uploads/2013/12/gastar-pedagio-orlando-disney-800x482.jpg",
      titulo: "Gastos Adicionais",
      desc: "Quais outros gastos podem ocorrer durante a viagem?",
      text:
          "– Gorjetas: nos EUA é de praxe deixar gorjeta (chamada de “tip”) para os prestadores de serviço. "
          "Nos restaurantes, deixa-se pelo menos 18% para o garçom. Ela é opcional mas é muito feio você não deixar já que os salários são baixos e os garçons contam com as gorjetas. "
          "O valor não vem na conta e o cálculo tem que ser feito por você e deixado na mesa depois que  pagar. "
          "Se tiver pagado com cartão, o recibo volta com um espaço para você escrever o valor da gorjeta que será debitada do seu cartão depois. "
          "Em grupos maiores do que 6 pessoas, a gorjeta é obrigatória e automaticamente adicionada a conta."
          "Gorjetas também são oferecidas para camareiras, carregadores de malas, taxistas e outros prestadores de serviço. Nestes casos, \$2 ou \$3 é de bom tamanho.\n\n"
          "– Pedágio: Você vai encontrar muitos em Orlando mas eles são baratos e alguns nem mesmo cobrador tem. "
          "Tenha sempre em mão (ou no console do carro) algumas moedas de \$0.25 (chamadas de quarter) para pagar os pedágios. "
          "Eles são sempre múltiplos e você vai encontrar valores de \$0.50, \$0.75, \$1, \$1.25 e \$1.50. "
          "Entre o aeroporto e a International Drive, você vai passar por pelo menos dois deles.\n"
          "Ah, e nos pedágios que não tem cobrador e cancela, tome muito cuidado para não passar sem pagar. "
          "Se fizer isso, pode ser parado por um policial ou ainda receber uma multa bem salgada depois da sua viagem "
          "(lembre-se que a locadora de automóveis tem todos os seus dados).\n\n"
          "– Estacionamentos: nos shoppings e outlets é de graça, e você só tem que pagar se quiser manobrista (em média \$20). "
          "Já nos parques, você terá que pagar estacionamento em todos eles, todos os dias, pelo menos \$25 por dia. Já deixe esse dinheiro separado.\n\n"
          "– Gasolina: depende muito quanto você andar, do tamanho e desempenho do carro. Em geral, com um carro médio, eu separo \$50/semana para abastecer o carro.\n\n"),
  Informations(
      img:
          "https://www.vaipradisney.com/blog/wp-content/uploads/2015/07/SEGURANCA_COMPRAS_4.jpg",
      titulo: "Segurança na hora de fazer compras",
      desc:
          "Em Orlando, tem muito malandro que sabe que o brasileiro enche as malas com produtos novos e encontram aí uma oportunidade para tirar vantagem.",
      text:
          "Orlando é uma delícia para curtir e comprar, mas nem por isso a gente deve deixar de ficar esperto. "
          "Em todo lugar você encontra gente boa e gente com má intenção, nos Estados Unidos não é diferente. "
          "Em Orlando, tem muito malandro que sabe que o brasileiro enche as malas com produtos novos e encontram aí uma oportunidade para tirar vantagem. Siga as dicas "
          "para não passar perrengue\n\n"
          "- Depois das compras vá direto para o hotel:\n\n"
          "Ir do shopping direto para o hotel é a melhor forma de evitar que te sigam e te abordem na sua próxima parada. "
          "Esse é um golpe comum no mundo todo: eles te seguem e te abordam depois ou simplesmente arrombam o seu carro, o que for mais fácil. "
          "Essa é a nossa recomendação mas se não for possível de maneira alguma ir direto para o hotel, tenha atenção redobrada para garantir que não está "
          "sendo seguido e coloque TODAS as compras no porta-malas. Assim pelo menos você chama menos atenção, né?\n\n"
          "Se comprar algo de valor, mesmo que perca mais tempo, a volta para o hotel é extremamente recomendada. "
          "Muita gente acha que está seguro de deixar compras de valores no estacionamento dos parques mas não está. "
          "Você não corre risco de ser roubado pelo funcionário do parque, mas por alguém que sabe o que você comprou e te seguiu até lá\n\n\n"
          "- Faça as compras de uma vez só: \n\n"
          "Para evitar ficar carregando as sacolas, muita gente prefere fazer paradas ao longo do dia e ir deixando as sacolas no porta malas do carro. "
          "Com essa prática, você está dando uma oportunidade fácil para que aquele ladrão que ia seguir alguém simplesmente arrombe seu carro e leve os produtos sem que "
          "ele precise sequer te abordar. Mais fácil pra ele! Para evitar ser vítima desse golpe, o recomendado é que leve todas as compras para o carro "
          "de uma vez só e de lá, direto pro hotel como falamos antes.\n\n"
          "E se eu levar uma mala para guardar as sacolas durante as compras?\n"
          "Isso pode parecer uma boa idéia mas eu não acho que seja. Tem quem ache que levar a mala para os shoppings e outlets resolve o problema de carregar as compras, "
          "mas esquece que isso é o maior chamariz da sua intenção de comprar muito. A mala chama atenção até de quem não quer roubar ninguém, imagina de quem quer! "
          "Mesmo que a mala esteja vazia, com certeza ela vai fazer você se destacar na multidão para os ladrões como uma vítima potencial.\n\n\n"
          "- Escolha bem onde estacionar:\n\n"
          "Como você já deve ter percebido, muitos roubos em Orlando acontecem logo que você sai das compras ou quando você deixa sacolas no carro e volta para dentro do shopping, "
          "ou seja, sempre perto do seu carro! Para evitar ser vítima desses golpes, uma estratégia boa é parar o carro sempre o mais próximo possível das cabines de segurança. "
          "Os ladrões procuram vítimas fáceis e com certeza assaltar alguém perto dos seguranças é sempre mais difícil.\n\n"
          "Outro bom ponto para deixar o carro é bem próximo de uma das entradas do shopping que você está visitando. "
          "Isso vai te permitir se movimentar rápido da saída do shopping até o carro e de lá, ir embora. Ou seja, pouco tempo de reação para que um ladrão possa te abordar.\n\n\n"
          "- Cuidado no hotel também: \n\n"
          "Mesmo nos hotéis é melhor não dar bobeira. Se você tem muito apego pelas suas aquisições, não deixe nada tão à vista. "
          "Mesmo em um bom hotel, não só funcionários mas hóspedes também podem ser oportunistas então busque não chamar atenção se estiver com itens de valor. "
          "Guarde eletrônicos e tudo aquilo que for caro no cofre ou na mala com cadeado.\n\n"
          "Outro ponto importante: não deixe sacolas e embalagens espalhadas pelo quarto, ou pior ainda, do lado de fora do quarto na expectativa de que"
          " um funcionário vá passar e jogar fora. Pode parecer bobagem mas excesso de sacolas e embalagens de produtos caros valem como um letreiro para "
          "os malandros dizendo “Aqui dentro tem itens ótimos, recém comprados para serem roubados”. Não custa nada se desfazer dessas coisas em um lixo mais central, né?\n\n"),
  Informations(
      img:
          "https://www.disneydenovo.com/wp-content/uploads/2018/02/sales-tax-orlando-1200x800_c.jpg?x23774",
      titulo: "Imposto em Orlando: Qual o valor da Sales Tax?",
      desc: "Como funciona o Imposto em Orlando? Qual é o valor?",
      text:
          "Quando você compra um produto em Orlando, você não vai pagar o preço que está na etiqueta pois terá que acrescentar o imposto sobre bens e serviços "
          "que é chamado de Sales Tax. No Brasil nós estamos acostumados com o imposto já vir incluído no preço do produto, enquanto em Orlando e outros "
          "lugares dos Estados Unidos o imposto é incluído após a compra. Por isso a diferença.\n\n"
          "O valor do imposto (Sales Tax) em Orlando é de aproximadamente 6,5% e essa regra muda em cada Estado americano. "
          "Por exemplo em Miami o valor é um pouco maior, em torno de 7%, e em New York vai para 8,9%.\n\n"
          "Ou seja, se o produto custa 100 dólares em uma loja em Orlando, você terá que pegar 106,5 dólares aproximadamente.\n\n"
          "O mesmo vale em um restaurante ou lanchonete, onde o valor que você vai pagar no final não é o mesmo que estava no cardápio pois terá que acrescentar o imposto.\n\n"),
];
