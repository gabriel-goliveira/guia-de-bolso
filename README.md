# Guia de Bolso #

Guia de Bolso is a Flutter app to help travelers going to Disney. It was made for study purpose.

### What technologies were used?###

* Flutter
* Dart
* Bloc
* Slidy


### What is the app prupose? ###

The app has a collection of information on what to do and where to go in Disney.

### Images ###

![picture](screenshots/home.png)

![picture](screenshots/lojas.png)

![picture](screenshots/loja.png)


